# Assigned Task

# Assingment-1
This folder contains practical_exercise_1 written in python 

# Assingment-2
This folder contains the project for practical_exercise_2 , written in RobotFramework.
Please follow the below setup and run the tests section.

To run robot framework tests python 2.* or python 3.* must be installed

### Setup
#### Python dependencies 
pip install robotframework-Selenium2Library

#### ChromeDriver
Please install chromedriver.exe file compatible with your installed chrome browser from
https://chromedriver.chromium.org/downloads

Create a new folder Drivers under project folder(Assingment-2) 
and place the downloaded chromedriver.exe file there 

### Run the tests
open the cmd from project folder and use the below command to run robot test case.

robot -d Results Tests/Ajio.robot
 
### Results
Please find the test results in the project Results folder.