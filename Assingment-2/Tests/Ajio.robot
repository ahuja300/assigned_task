*** Settings ***
Resource    ../Resources/Keywords.robot


Test Setup      Begin Web Test
Test Teardown   End Web Test



*** Test Cases ***
Adding two highest price product and verifying the cart
    [Documentation]    Test case to verify that user is able to add two highest price products from a certain category into the cart
    [Tags]  Current
    Verify Home Page Title
    Select category
    Set Dropdown Filter High to Low
    Select 2 Highest Price Products
    Verify Items In the Bag



