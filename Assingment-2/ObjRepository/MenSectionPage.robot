*** Variables ***
${BROWSER} =                chrome
${START_URL} =              https://www.ajio.com/shop/men
${MEN_SECTION} =            //a[@href='/shop/men']
${CATEGORY_KURTAS}=         //*[@href='/s/kurtas-for-men' and @title='Kurtas']
${ALL_PRODUCTS} =           //*[@class='item rilrtl-products-list__item item']
${SELECT_SIZE}=             //div[@class='size-swatch']/div[text()='S']
${SELECT_DROPDOWN_SORTBY}=  //div[@class='filter-dropdown']/select
${SORYBY_VALUE_HIGHTOLOW}=  prce-desc
${QUICK_WAIT_TIME}=         10
${HOME_PAGE_TITLE}=         Online Shopping for Men – Clothing, Footwear, Fashion & Accessories | AJIO
