*** Settings ***
Library     Selenium2Library
Library     String
Library     OperatingSystem
Resource    ../ObjRepository/MenSectionPage.robot
Resource    ../ObjRepository/CartPage.robot


*** Keywords ***
End Web Test
    close browser

Begin Web Test
   ${curr_dir}=   replace string   ${EXEC_DIR}    \\      /
   create webdriver    Chrome   executable_path=${curr_dir}/Drivers/chromedriver.exe
   set screenshot directory    ${curr_dir}/Screenshots
   go to    ${START_URL}
   maximize browser window

Verify Home Page Title
   wait until page contains    Shop By Product     ${QUICK_WAIT_TIME}
   title should be          ${HOME_PAGE_TITLE}
   log to console       Home page title has been verified

Select Category
    set selenium speed  1s
    mouse over       ${MEN_SECTION}
    mouse over       ${CATEGORY_KURTAS}
    click element    ${CATEGORY_KURTAS}
    capture page screenshot

Set Dropdown Filter High to Low
    select from list by value   ${SELECT_DROPDOWN_SORTBY}   ${SORYBY_VALUE_HIGHTOLOW}
    capture page screenshot

Select ${number_of_products} Highest Price Products
    @{data}=    Get WebElements     ${ALL_PRODUCTS}
    : FOR  ${i}  IN RANGE   0     ${number_of_products}
    \    ${value}=          Get WebElement   @{data}[${i}]
    \    click element      ${value}
    \    select window      NEW
    \    click element      ${SELECT_SIZE}
    \    click element      ${ADD_TO_BAG}
    \    close window
    \    select window      MAIN

Verify Items In the Bag
    click element   ${BAG_ICON}
    wait until element is visible   ${ITEMS_IN_BAG}
    ${status}=       run keyword and return status   page should contain element    ${ITEMS_IN_BAG}   limit=2
    capture page screenshot
    run keyword if  ${status}       log to console      two products successfully added into the card
    ...  ELSE       fail        Test Case failed